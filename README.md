# openasmat

Aide à la gestion des heures des assistantes maternelles

## Prés requit
Python 3  
Django 2  
postgresql 10  

## Configuration
### Base de donnée
se connecter en utilisateur postgres
```bash
sudo -i -u postgres
```
Créer un utilisateur **openasmat**  
```bash
postgre$ createuser -W openasmat
```
Créer une base de donnée **openasmat**  
```bash
postgre$ createdb -O openasmat -e openasmat
```
Une autre base **openasmat_test** est utilisé pour les tests unitaires  
Pour que cela fonctionne, il faut que votre utilisateur est les droits pour créer des bases de données.
```sql
ALTER USER openasmat CREATEDB;
```

Les nom des tables peuvent être changé dans `openasmat_django/openasmat_django/settings.py`  

La lecture du mot de passe et du reste de la configuration est lu dans `~/.pgpass`  
Vous pouvez définir un autre emplacement de `.pgpass`, dans votre venv python par example, à l'aide de la variable d’environnement **PGPASSFILE**
```bash
PGPASSFILE=../.pgpass
export PGPASSFILE
```

### Environnement virtuel python
Créer un environnement virtuel python (venv)
```bash
python3 -m venv /venev
```
activer l'environnement virtuel
```bash
source venv/bin/activate
```
installer les modules supplémentaires
```bash
pip install -r requierements.txt
```

### Sauvegarde et restauration
Sauvegarde (dump) avec la date du jour
```bash
manage.py dumpdata > $(date +%Y-%m-%d)_dump.json
```
Restauration
```bash
manage.py loaddata 2019-01-29_dump.json 
```
Les sauvegardent peuvent être automatissé en créant une tache cron pointant vers le script **openasmat.cron**.  
Il faut éditer les répertoires en fonction de votre configuration.
