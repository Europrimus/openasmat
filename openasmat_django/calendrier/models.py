from django.db import models

import datetime
import calendar

import locale
from jours_feries_france.compute import JoursFeries
from vacances_scolaires_france import SchoolHolidayDates

class Calendrier(models.Model):
    Contrat = models.ForeignKey(
        'contrat.Contrat',
        on_delete=models.PROTECT,
    )
    Type = models.ForeignKey(
        'TypeTravail',
        on_delete=models.PROTECT,
    )
    date = models.DateField('date')
    heuresRealise = models.DecimalField(max_digits=4, decimal_places=2)
    indemEntretien = models.BooleanField(default=False)
    indemRepas = models.BooleanField(default=False)
    indemGouter = models.BooleanField(default=False)
    indemKm = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    valideRepresentantLegal = models.BooleanField(blank=True, null=True)
    valideAsmat = models.BooleanField(blank=True, null=True)
    class Meta:
        db_table = 'calendrier'
        get_latest_by = 'date'
        ordering = ('date',)
        unique_together = ('date','Contrat','Type')

    def __str__(self):
        return "%s (%s) %sh - E:%s"% (self.date, self.Type, self.heuresRealise, self.indemEntretien)

    def getJoursMois(Annee,Mois):
        """
        getJoursMois(Annee,Mois)
        Renvois les jours du Mois
        """
        locale.setlocale(locale.LC_ALL, '')
        joursFerier = JoursFeries.for_year(Annee).values()
        SchoolHolidayDatesObject = SchoolHolidayDates()
        weekend = ['0','6']
        datesMois = [False]
        # (datetime.date(Annee,Mois,1) + datetime.timedelta(days=x) \
        for j in range(1,calendar.monthrange(Annee,Mois)[1]+1):
            jour = datetime.date(Annee,Mois,j)
            datesMois.append({
                "date":jour.strftime('%d %A'),
                "estWeekend":(jour.strftime('%w') in weekend),
                "estFerie":(jour in joursFerier),
                "estVacancesZoneA":SchoolHolidayDatesObject.is_holiday_for_zone(jour, 'A'),
                "estVacancesZoneB":SchoolHolidayDatesObject.is_holiday_for_zone(jour, 'B'),
                "estVacancesZoneC":SchoolHolidayDatesObject.is_holiday_for_zone(jour, 'C'),
                "contrats":[],
            })
        return datesMois

    def getEnregistrementMois(ListIdContrats,Annee,Mois):
        """
        getEnregistrementMois(ListIdContrats,Annee,Mois)
        Renvois les contrat du mois
        avec :
         - ListIdContrats : laliste des id des contrats a prendre en compte
         - Annee,Mois : l'année et le mois à prendre en compte
        """
        return Calendrier.objects \
            .filter(Contrat__in=ListIdContrats) \
            .filter(date__year=Annee,date__month=Mois)

    def getTotalMois(QuerySetIdContrats,Annee,Mois):
        """
        getTotalMois(ListIdContrats,Annee,Mois)
        Renvois pour un mois:
        - total des heures rémunérées
        - total des heures par catégorie
        - nombre d'indemnité d'entretien
        - nombre d'indemnité de repas
        - nombre d'indemnité de gouter
        - total des indemnité kilometrique
        """
        total={}
        for Contrat in QuerySetIdContrats:
          QuerySetMois = Calendrier.objects \
                  .filter(Contrat__id=Contrat.id) \
                  .filter(date__year=Annee,date__month=Mois)
          total[Contrat.id]={
              'nom':Contrat,
              'couleur':Contrat.couleur,
              'total':QuerySetMois.aggregate(
                      HeuresRemunere=models.Sum('heuresRealise'),
                      IndemKm=models.Sum('indemKm')
                  ),
              'nbIndemEntretien':QuerySetMois.filter(indemEntretien=True).count(),
              'nbIndemRepas':QuerySetMois.filter(indemRepas=True).count(),
              'nbIndemGouter':QuerySetMois.filter(indemGouter=True).count(),
          }
        return total

    def moisSuivant(annee, mois):
      suivantAnnee = annee
      suivantMois = mois + 1
      if mois == 12:
          suivantMois = 1
          suivantAnnee = annee +1
      return suivantAnnee, suivantMois
      
    def moisPrecedent(annee, mois):
      precedentAnnee = annee
      precedentMois = mois - 1
      if mois == 1:
          precedentMois = 12
          precedentAnnee = annee - 1
      return precedentAnnee, precedentMois

class TypeTravail(models.Model):
    nom = models.CharField(max_length=30)
    sigle = models.CharField(max_length=3)
    description = models.CharField(max_length=300, blank=True, null=True)

    def __str__(self):
        return "%s"% (self.sigle)
