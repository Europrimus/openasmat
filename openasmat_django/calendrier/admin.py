from django.contrib import admin

from .models import Calendrier
from .models import TypeTravail

admin.site.register(Calendrier)
admin.site.register(TypeTravail)
