# import Django
from django.shortcuts import render
from django.shortcuts import HttpResponse, HttpResponseRedirect, reverse
from django.shortcuts import get_object_or_404
from django.http import Http404
# authentification
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# les models openasmat
from contrat.models import Contrat
from .models import Calendrier, TypeTravail
from tiers.models import *

# import python général
import datetime

@login_required(login_url='/tiers')
def index(request):
    aujourdui=datetime.datetime.now()
    context={
        'message':"",
        'Annee':aujourdui.year,
        'Mois':aujourdui.month,
    }
    return render(request, 'calendrier/index.html', context)

@login_required(login_url='/tiers')
def mois(request,annee,mois):
    if mois < 1 or mois > 12:
        raise Http404("mois non reconnu")
    Contrats=Contrat.get(request.user.id, annee, mois)
    if Contrats.count()<1:
        context={
            'message':"Pasde contrat"
        }
        return HttpResponseRedirect(reverse('Contrat:index'),context)
# on récupère les données du mois
    try:
        ActiviteeJours = Calendrier.getEnregistrementMois(Contrats,annee,mois)
    except Calendrier.DoesNotExist:
        raise Http404("Erreur 404")
# dates suivante et précédente
    suivantAnnee, suivantMois = Calendrier.moisSuivant(annee,mois)
    precedentAnnee, precedentMois = Calendrier.moisPrecedent(annee,mois)

# Ajouts des activitées pour chaque jour
    joursMois = Calendrier.getJoursMois(annee,mois)
    #listIdContrat = list(Contrats.values_list('id',flat=True))
    listIdContrat = {}
    for id, couleur in list(Contrats.values_list('id', 'couleur')):
      listIdContrat.setdefault(id, []).append(couleur)
    listIdContratJours={}
    for j in ActiviteeJours:
      # le jour du mois a traiter
      jour = int(float(j.date.strftime('%d')))
      
      # si le jour n'est pas dans la liste, on l'ajoute à la liste
      if jour not in listIdContratJours:
        listIdContratJours[jour]=[]
        nextIndex = 0
     
      for i in range(nextIndex, list(listIdContrat.keys()).index(j.Contrat.pk)):
        joursMois[ jour ]['contrats'].append({})
      
      # si le contrat n'a pas été traitéer pour ce jour
      if j.Contrat.pk not in listIdContratJours[jour]:
        joursMois[ jour ]['contrats'].append( {
            'contratId':j.Contrat.pk,
            'contratNom':j.Contrat,
            'heuresRealise':j.heuresRealise,
            'TypeSigle':j.Type.sigle,
            'TypeNom':j.Type.nom,
            'indemEntretien':j.indemEntretien,
            'indemRepas':j.indemRepas,
            'indemGouter':j.indemGouter,
            'indemKm':j.indemKm,
            'ContratCouleur':listIdContrat[j.Contrat.pk][0],
        })
        listIdContratJours[jour].append(j.Contrat.pk)
        nextIndex=list(listIdContrat.keys()).index(j.Contrat.pk)+1

    context = {
        'joursMois': joursMois,
        'sousTitre':datetime.date(annee,mois,1).strftime('%B %Y'),
        'mois':mois,
        'precedentMois':precedentMois,
        'suivantMois':suivantMois,
        'annee':annee,
        'precedentAnnee':precedentAnnee,
        'suivantAnnee':suivantAnnee,
        'utilisateur':request.user,
        'Contrats':Contrats,
        'listeTypeTravail': TypeTravail.objects.all(),
        'date':datetime.date(annee,mois,1),
        'total':Calendrier.getTotalMois(Contrats,annee,mois),
    }
    return render(request, 'calendrier/mois.html', context)

@login_required(login_url='/tiers')
def ajouterDate(request):
    '''
    Affiche le formulaire d'ajout de date
    '''
    date = datetime.date.today().isoformat()
    context = {
        'date': date,
        'listeTypeTravail': TypeTravail.objects.all(),
        'utilisateurId':request.user.id,
        'utilisateurNom':request.user.username,
        'Contrats':Contrat.getContrats(request.user.id),
    }
    return render(request, 'calendrier/ajouter.html', context)

@login_required(login_url='/tiers')
def sauverDate(request):
    '''
    Valide et enregistre la date
    '''
    acceptedParam={
        'contrat':'contrat',
        'typeTravail':'typeTravail',
        'date':'date',
        'heure':None,
        'indemEntretien':False,
        'indemRepas':False,
        'indemGouter':False,
        'indemKm':0,
        'valideRepresentantLegal': False,
        'valideAsmat': False,
    }
    try:
        for key, val in request.POST.items():
            if key in acceptedParam:
                if val=='on':
                    val=True;
                if val=='':
                    val=False;
                acceptedParam[key]=val

    except(KeyError):
        context = acceptedParam
        context['listeTypeTravail'] = TypeTravail.objects.all()
        context['Contrats'] = Contrat.getContrats(request.user.id)
        return render(request, 'calendrier/ajouter.html', context)
    else:
        date = Calendrier(
            Contrat = Contrat(id=acceptedParam['contrat']),
            Type = TypeTravail(id=acceptedParam['typeTravail']),
            date = acceptedParam['date'],
            heuresRealise = acceptedParam['heure'],
            indemEntretien = acceptedParam['indemEntretien'],
            indemRepas = acceptedParam['indemRepas'],
            indemGouter = acceptedParam['indemGouter'],
            indemKm = acceptedParam['indemKm'],
            valideRepresentantLegal = acceptedParam['valideRepresentantLegal'],
            valideAsmat = acceptedParam['valideAsmat'],
        )
        date.save()
        return HttpResponseRedirect(
            reverse('Calendrier:mois',
                kwargs={'annee':date.date[0:4],'mois':date.date[5:7]})
            )
