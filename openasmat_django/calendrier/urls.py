from django.urls import path

from . import views

app_name = 'Calendrier'
urlpatterns = [
    # /calendrier
    path('', views.index, name='index'),
    # /calendrier/2018/05
    path('<int:annee>/<int:mois>', views.mois, name='mois'),
    path('ajouter', views.ajouterDate, name='ajouterDate'),
    path('sauver', views.sauverDate, name='sauverDate'),
]
