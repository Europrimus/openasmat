# import Django
from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import HttpResponseRedirect
from django.shortcuts import reverse

# django authentification
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# les models openasmat
from contrat.models import Contrat

def index(request):
    if request.user.is_authenticated:
        return HttpResponse("tiers is_authenticated")
    else :
        return identification(request)

def identification(request):
    context={

    }
    return render(request, 'tiers/identification.html', context)

def verifierIdentification(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request,
        username=username,
        password=password)
    if user is not None:
        login(request, user)
        try:
            return HttpResponseRedirect(request.GET['next'])
        except:
            return HttpResponseRedirect(reverse('Calendrier:index'))
    else:
        context={
            'message':"Erreur d'identification"
        }
        return render(request, 'tiers/identification.html', context)

@login_required(login_url='/tiers')
def deconnexion(request):
    logout(request)
    return identification(request)
