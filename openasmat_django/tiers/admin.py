from django.contrib import admin

from .models import Tiers
from .models import Type
from .models import Adresse

admin.site.register(Tiers)
admin.site.register(Type)
admin.site.register(Adresse)
