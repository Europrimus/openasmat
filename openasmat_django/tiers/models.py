from django.db import models
from django.contrib.auth.models import User

class Tiers(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    Type = models.ForeignKey(
        'Type',
        on_delete=models.PROTECT,
    )
    User = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    dateNaissance = models.DateField('date', blank=True, null=True)
    email = models.CharField(max_length=30, blank=True, null=True)
    telPortable = models.CharField(max_length=15, blank=True, null=True)
    telFixe = models.CharField(max_length=15, blank=True, null=True)
    Adresse =  models.ForeignKey(
        'Adresse',
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )
    class Meta:
        db_table = 'tiers'
        verbose_name_plural = 'tiers'
        unique_together = (
            ('User',),
            ('nom','prenom','dateNaissance',),
        )

    def __str__(self):
        return "%s %s"% (self.nom, self.prenom)

class Type(models.Model):
    nom = models.CharField(max_length=30)
    description = models.CharField(max_length=300, blank=True, null=True)

    def __str__(self):
        return "%s"% (self.nom)

class Adresse(models.Model):
    ligne1 = models.CharField(max_length=50)
    ligne2 = models.CharField(max_length=50, blank=True, null=True)
    ligne3 = models.CharField(max_length=50, blank=True, null=True)
    codePostale = models.IntegerField()
    ville = models.CharField(max_length=50)

    def __str__(self):
        return "%s - %s %s"% (self.ligne1,self.codePostale,self.ville)
