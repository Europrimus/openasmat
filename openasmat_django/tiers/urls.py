from django.urls import path

from . import views

app_name = 'Tiers'
urlpatterns = [
    # /tiers
    path('', views.index, name='Index'),
    path('connexion', views.verifierIdentification, name='login'),
    path('deconnexion', views.deconnexion, name='logout'),
    # /tiers/123
    #path('<int:idtiers>', views.tiers, name='tiers'),
]
