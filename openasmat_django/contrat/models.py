from django.db import models
from django.db.models import Q
#from django.contrib.postgres.indexes import GistIndex
from colorful.fields import RGBColorField

from calendrier.models import Calendrier

class Contrat(models.Model):
    Enfant = models.ForeignKey(
        'tiers.Tiers',
        related_name='enfant',
        on_delete=models.PROTECT,
    )
    RepresentantLegal1 = models.ForeignKey(
        'tiers.Tiers',
        related_name='representantLegal1',
        on_delete=models.PROTECT,
    )
    RepresentantLegal2 = models.ForeignKey(
        'tiers.Tiers',
        related_name='representantLegal2',
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    Asmat = models.ForeignKey(
        'tiers.Tiers',
        related_name='asmat',
        on_delete=models.PROTECT,
    )
    dateDebut = models.DateField('date')
    dateFin = models.DateField('date', blank=True, null=True)
    nbSemaineAnnuel = models.IntegerField(default=46)
    tauxHorraireNet = models.DecimalField(max_digits=4, decimal_places=2)
    indemEntretien = models.DecimalField(max_digits=4, decimal_places=2)
    indemRepas = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    indemGouter = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    indemKm = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    description = models.CharField(max_length=300, blank=True, null=True)
    url = models.CharField(max_length=300, blank=True, null=True)
    couleur = RGBColorField(default=None, null=True)
    class Meta:
        db_table = 'contrat'
        get_latest_by = 'dateDebut'
        ordering = ('dateDebut',)
        unique_together = ('Enfant','Asmat','dateDebut')
#        indexes = [
#            GistIndex(fields=['Enfant', 'Asmat'],),
#        ]
# https://www.cybertec-postgresql.com/en/postgresql-exclude-beyond-unique/
# https://thebuild.com/presentations/django-pg-pgconf.pdf
# ALTER TABLE public.contrat ADD EXCLUDE USING gist ( Enfant_id WITH =, Asmat_id WITH =, range(dateDebut,dateFin) WITH && );


    def __str__(self):
        return "%s - %s"% (self.Enfant,self.Asmat)

    def get(userId, annee=False, mois=False):
        contrats = Contrat.objects.filter( 
            Q(Enfant__User=userId) |
            Q(RepresentantLegal1__User=userId) |
            Q(RepresentantLegal2__User=userId) |
            Q(Asmat__User=userId)
          )

        if annee and mois:
          SuivantAnnee , suivantMois = Calendrier.moisSuivant(annee,mois)
          contrats = contrats.filter(
            Q(dateDebut__lte = "{:04d}-{:02d}-01".format(SuivantAnnee, suivantMois),
              dateFin__gte = "{:04d}-{:02d}-01".format(annee, mois)) |
            Q(dateDebut__lte = "{:04d}-{:02d}-01".format(SuivantAnnee, suivantMois),
              dateFin__isnull=True)
            )
        return contrats

